:: OpenVPN beenden
taskkill /IM openvpn.exe /F
taskkill /IM openvpn-gui.exe /F

:: OpenVPN starten
cd "C:\Program Files\OpenVPN\config"
START /B ../bin/openvpn.exe --config "C:\Program Files\OpenVPN\config\bvpn_austria_tcp.ovpn"
:: timeout /T 10 > nul

:: CMD beenden
:: taskkill /IM cmd.exe /F
