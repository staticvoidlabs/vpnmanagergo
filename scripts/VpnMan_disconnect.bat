:: VLC beenden
taskkill /IM vlc.exe /F

:: OpenVPN beenden
taskkill /IM openvpn.exe /F
taskkill /IM openvpn-gui.exe /F

:: CMD beenden
taskkill /IM cmd.exe /F
