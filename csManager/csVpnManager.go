package csmanager

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"time"

	Models "../csModel"
)

func InitVpnManagerMenuItemsRepo() Models.VpnMenuItemsRepo {

	// Initialize the shows array.
	var menuItems Models.VpnMenuItemsRepo

	jsonFile, _ := os.Open("VpnMenuItems.json")
	byteValue, _ := ioutil.ReadAll(jsonFile)

	// Unmarshal the byte array which contains the json content into 'menuItems.'
	json.Unmarshal(byteValue, &menuItems)

	return menuItems
}

func InitVpnConnection(menuItem Models.VpnMenuItem, config Models.Config) {

	// Execute scheduled task: C:\Windows\System32\schtasks.exe /run /tn "VpnMan_connect"
	// Scheduled task program: C:\windows\system32\cmd.exe
	// Scheduled task args: /c start /min "" "C:\Daten\vpn_livetv\Batch\Scripts\VpnMan_connect.bat"

	// First quit existing vpn connection.
	quitVpnConnections()

	if menuItem.VpnCommand == "VpnMan_connect" {

		// C:\Windows\System32\schtasks.exe /run /tn "VpnMan_connect"
		tmpArg1 := "/run"
		tmpArg2 := "/tn"
		tmpArg3 := "VpnMan_connect"

		cmd := exec.Command("C:\\Windows\\System32\\schtasks.exe", tmpArg1, tmpArg2, tmpArg3)
		var out bytes.Buffer
		var stderr bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &stderr
		err := cmd.Run()
		if err != nil {
			fmt.Println(fmt.Sprint(err) + ": " + stderr.String())
			return
		}
		fmt.Println("Result: " + out.String())

		// Wait some time and start VLC playback.
		/*
			tmpDelaySecCounter := 0

			for tmpDelaySecCounter < 10 {

				tmpIsConnected := getCurrentIpAddress()

				if !tmpIsConnected {
					time.Sleep(1 * time.Second)
					fmt.Println("Waiting for VPN connection for " + strconv.Itoa(tmpDelaySecCounter) + " sec...")
				}

				tmpDelaySecCounter++
			}

			fmt.Println("Connected. Will start playback now.")
		*/

		tmpDelayPlayback, err := strconv.Atoi(config.DelayVlcPlayback)

		if err != nil {
			tmpDelayPlayback = 7
		}

		time.Sleep(time.Duration(tmpDelayPlayback) * time.Second)
		go StartPlayback(menuItem, config.FullPathVlc)

	}

}

func quitVpnConnections() {

	// Execute scheduled task: C:\Windows\System32\schtasks.exe /run /tn "VpnMan_disconnect"
	// Scheduled task program: C:\windows\system32\cmd.exe
	// Scheduled task args: /c start /min "" "C:\Daten\vpn_livetv\Batch\Scripts\VpnMan_disconnect.bat"

	tmpArg1 := "/run"
	tmpArg2 := "/tn"
	tmpArg3 := "VpnMan_disconnect"

	cmd := exec.Command("C:\\Windows\\System32\\schtasks.exe", tmpArg1, tmpArg2, tmpArg3)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + ": " + stderr.String())
		return
	}
	fmt.Println("Result: " + out.String())
}

func QuitVpnManInstances() {

	// Execute scheduled task: C:\Windows\System32\schtasks.exe /run /tn "VpnMan_quitApp"
	// Scheduled task program: C:\windows\system32\cmd.exe
	// Scheduled task args: /c start /min "" "C:\Daten\vpn_livetv\Batch\Scripts\VpnMan_quitApp.bat"

	tmpArg1 := "/run"
	tmpArg2 := "/tn"
	tmpArg3 := "VpnMan_quitApp"

	cmd := exec.Command("C:\\Windows\\System32\\schtasks.exe", tmpArg1, tmpArg2, tmpArg3)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + ": " + stderr.String())
		return
	}
	fmt.Println("Result: " + out.String())
}
