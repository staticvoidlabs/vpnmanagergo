package csmanager

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	Models "../csModel"
)

var (
	EpgOrf1Now          string
	EpgOrf1Next         string
	EpgOrf2Now          string
	EpgOrf2Next         string
	EpgOrf3Now          string
	EpgOrf3Next         string
	EpgOrfSportPlusNow  string
	EpgOrfSportPlusNext string
)

/*
https://xmltv.xmltv.se/orf1.orf.at_2020-09-17.xml
https://xmltv.xmltv.se/orf2.orf.at_2020-09-17.xml
https://xmltv.xmltv.se/orf3.orf.at_2020-09-17.xml
https://xmltv.xmltv.se/sportplus.orf.at_2020-09-17.xml
*/

// GetEpgData tries to get EPG data from xmltv.se.
func GetEpgData() {

	EpgOrf1Now = "n/a"
	EpgOrf1Next = "n/a"
	EpgOrf2Now = "n/a"
	EpgOrf2Next = "n/a"
	EpgOrf3Now = "n/a"
	EpgOrf3Next = "n/a"
	EpgOrfSportPlusNow = "n/a"
	EpgOrfSportPlusNext = "n/a"

	EpgOrf1Now, EpgOrf1Next = getShows("https://xmltv.xmltv.se/orf1.orf.at")
	EpgOrf2Now, EpgOrf2Next = getShows("https://xmltv.xmltv.se/orf2.orf.at")
	EpgOrf3Now, EpgOrf3Next = getShows("https://xmltv.xmltv.se/orf3.orf.at")
	EpgOrfSportPlusNow, EpgOrfSportPlusNext = getShows("https://xmltv.xmltv.se/sportplus.orf.at")

}

func getXML(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return []byte{}, fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return []byte{}, fmt.Errorf("Status error: %v", resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, fmt.Errorf("Read body: %v", err)
	}

	return data, nil
}

func getShows(url string) (showNow, showNext string) {

	tmpURL := url + "_" + time.Now().Format("2006-01-02") + ".xml"

	xmlBytes, err := getXML(tmpURL)
	if err != nil {
		fmt.Println(err)
	} else {

		var result Models.EpgData
		err = xml.Unmarshal(xmlBytes, &result)
		if err != nil {
			fmt.Println(err)
		}

		// Build datetime string to find needed entries.
		currentTime, err := strconv.Atoi(time.Now().Format("20060102150405"))
		if err != nil {
			fmt.Println(err)
		}

		// Update found show infos with a clean start time.
		for i, e := range result.Programmes {

			tmpDateTimeCurrentShow, err := time.Parse("20060102150405", e.Start[0:14])
			if err != nil {
				fmt.Println(err)
			}

			// Add 2 hours to fit our timezone.
			tmpDateTimeCurrentShow = tmpDateTimeCurrentShow.Add(time.Hour*2 + time.Minute*1 + time.Second*1)

			result.Programmes[i].StartClean, err = strconv.Atoi(tmpDateTimeCurrentShow.Format("20060102150405")[0:14])
			if err != nil {
				fmt.Println(err)
			}

		}

		// Look for current and next show.
		for i, e := range result.Programmes {

			if i < len(result.Programmes)-1 {

				if currentTime > e.StartClean && currentTime < result.Programmes[i+1].StartClean {

					showNow = getCleanTime(result.Programmes[i].StartClean) + e.Title
					showNext = getCleanTime(result.Programmes[i+1].StartClean) + result.Programmes[i+1].Title

					continue
				}

			}

		}

	}

	return showNow, showNext
}

func getCleanTime(startTime int) string {

	tmpHour := strconv.Itoa(startTime)[8:10]
	tmpMin := strconv.Itoa(startTime)[10:12]

	return tmpHour + ":" + tmpMin + " Uhr   "
}
