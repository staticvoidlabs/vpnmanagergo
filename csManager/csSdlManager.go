package csmanager

import (
	"fmt"
	"strconv"

	Models "../csModel"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

var err error
var Window *sdl.Window
var Renderer *sdl.Renderer
var FontShowItem *ttf.Font
var FontHeaderVpnManager *ttf.Font
var FontHeaderCouchPlayer *ttf.Font
var WindowHeight int32
var WindowWidth int32
var windowPosX int32
var windowPosY int32

// InitSdlSubsysten initialized SDL.
func InitSdlSubsysten(config *Models.ConfigSdl) error {

	// Init SDL.
	err := sdl.Init(sdl.INIT_EVERYTHING)

	if err != nil {
		fmt.Println("Error initializing SDL: ", err)
	}

	windowPosX = 0
	windowPosY = 0

	setWindowDimensions(config)
	Window = createWindow(config)
	Renderer = createRenderer(Window)
	initFonts(config)

	return err

}

func setWindowDimensions(config *Models.ConfigSdl) {

	// Get current screen resolution.
	mode, err := sdl.GetCurrentDisplayMode(0)

	if err != nil {
		fmt.Println("Error getting current display mode: ", err)
		return
	}

	if (mode.H / 3) > 288 {
		WindowHeight = (mode.H / 3)
	} else {
		WindowHeight = 288
	}

	WindowWidth = mode.W / 3 * 2

	windowPosX = 0
	windowPosY = WindowHeight * -1

	fmt.Println("Width: " + strconv.Itoa(int(WindowWidth)) + " / Height: " + strconv.Itoa(int(WindowHeight)))
}

func createWindow(config *Models.ConfigSdl) *sdl.Window {

	sdlflags := uint32(sdl.WINDOW_OPENGL)
	sdlflags |= sdl.WINDOW_BORDERLESS

	// Create main window.
	Window, err = sdl.CreateWindow(
		"CouchPlayer", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, WindowWidth, WindowHeight, sdlflags)

	if err != nil {
		fmt.Println("Error creating SDL window: ", err)
	}

	// Set window opacity.
	err = Window.SetWindowOpacity(config.WindowOpacity)

	if err != nil {
		fmt.Println("Error setting opacity: ", err)
	}

	// Set SDL windows icon.
	img.Init(img.INIT_JPG)
	surfaceImg, _ := img.Load("images/Icon_Mediaserver.png")
	Window.SetIcon(surfaceImg)

	return Window
}

func createRenderer(window *sdl.Window) *sdl.Renderer {

	// Create renderer.
	Renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)

	if err != nil {
		fmt.Println("Error creating renderer: ", err)
	}

	return Renderer

}

func initFonts(config *Models.ConfigSdl) {

	// Init and open fonts.
	if err := ttf.Init(); err != nil {
		fmt.Printf("Failed to initialize TTF: %s\n", err)
	}

	if FontShowItem, err = ttf.OpenFont("fonts/calibril.ttf", 20); err != nil {
		fmt.Printf("Failed to open font: %s\n", err)
	}

	if FontHeaderVpnManager, err = ttf.OpenFont("fonts/calibril.ttf", 30); err != nil {
		fmt.Printf("Failed to open font: %s\n", err)
	}

	if FontHeaderCouchPlayer, err = ttf.OpenFont("fonts/calibril.ttf", 40); err != nil {
		fmt.Printf("Failed to open font: %s\n", err)
	}

}

func UpdateWindowPosition(windowsDelayFactor float32) {

	if windowPosY < -10 {
		windowPosY = windowPosY + int32((10 * windowsDelayFactor))
	}

	windowPosX = WindowWidth / 4

	Window.SetPosition(windowPosX, windowPosY)
}
