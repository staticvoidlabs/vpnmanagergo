package csmanager

import (
	"bytes"
	"fmt"
	"net"
	"os"
	"os/exec"

	Models "../csModel"
)

func killAllPlayerInstances() {

	fmt.Println("Terminating player instances.")

	cmd := exec.Command("taskkill", "/IM", "vlc.exe")

	var out bytes.Buffer
	var stderr bytes.Buffer

	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()

	if err != nil {
		// "Der Prozess "vlc.exe" wurde nicht gefunden"
		fmt.Println("Error: " + stderr.String())
		return
	}

}

func DeleteShowFile(showItem *Models.ShowItem) {

	var err = os.Remove(showItem.FullPath)

	fmt.Println("Deleting show: " + showItem.FullPath)

	if err != nil {
		fmt.Println("Error: " + err.Error())
		return
	}

}

func getCurrentIpAddress() bool {

	tmpIsAustrianIp := false

	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				//os.Stdout.WriteString(ipnet.IP.String() + "\n")
				//fmt.Println(ipnet.IP.String()[0:5])
				if ipnet.IP.String()[0:6] == "10.80." {
					tmpIsAustrianIp = true
				}
			}
		}
	}

	return tmpIsAustrianIp
}
