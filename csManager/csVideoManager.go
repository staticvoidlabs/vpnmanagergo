package csmanager

import (
	"bytes"
	"fmt"
	"os/exec"

	Models "../csModel"
)

// StartPlayback starts playback of given show in VLC.
func StartPlayback(menuItem Models.VpnMenuItem, pathVlc string) {

	// First exit all running instances of VLC.
	//killAllPlayerInstances()

	//pathVlc := "C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe"
	tmpArg1 := "--fullscreen"
	tmpArg2 := menuItem.PlaybackUrl

	cmd := exec.Command(pathVlc, tmpArg1, tmpArg2)

	var out bytes.Buffer
	var stderr bytes.Buffer

	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()

	fmt.Println("Starting playback of Url: " + menuItem.PlaybackUrl)

	if err != nil {
		fmt.Println("Error: " + stderr.String())
		fmt.Println("Error: ")
		return
	}

}
