package csmodel

import "github.com/veandco/go-sdl2/sdl"

// VpnMenuItem represents a show item.
type VpnMenuItem struct {
	ID          int
	Text        string
	Country     string
	Icon        string
	IconSmall   string
	VpnCommand  string
	PlaybackUrl string
}

// C:\Windows\System32\schtasks.exe /run /tn "ORF"

// SdlVpnMenuItem represents SDL related data for a VpnMenuItem.
type SdlVpnMenuItem struct {
	IsSelected      bool
	Surface         *sdl.Surface
	SurfaceSelected *sdl.Surface
	SurfaceEpgNow   *sdl.Surface
	SurfaceEpgNext  *sdl.Surface
	Texture         *sdl.Texture
	TextureSelected *sdl.Texture
	TextureEpgNow   *sdl.Texture
	TextureEpgNext  *sdl.Texture
}
