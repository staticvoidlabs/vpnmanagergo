package csmodel

type Config struct {
	Version          string    `json:"version"`
	DebugLevel       string    `json:"debugLevel"`
	ShowListStyle    string    `json:"showListStyle"`
	ScanFolder       string    `json:"folderToScan"`
	FullPathFfmpeg   string    `json:"pathFfmpeg"`
	FullPathFfProbe  string    `json:"pathFfprobe"`
	FullPathVlc      string    `json:"pathVlc"`
	OptionsVlc       string    `json:"optionsVlc"`
	DelayVlcPlayback string    `json:"delayVlcPlayback"`
	SdlConfig        ConfigSdl `json:"sdlConfig"`
}

type ConfigSdl struct {
	OffsetHeader       int32   `json:"offsetHeader"`
	WindowOpacity      float32 `json:"windowOpacity"`
	ScreenHeight       int32   `json:"screenHeight"`
	ScreenWidth        int32   `json:"screenWidth"`
	WindowsDelayFactor float32 `json:"windowsDelayFactor"`
}

/*

// STRUCT
type Config struct {
	Database struct {
		Host     string `json:"host"`
		Password string `json:"password"`
	} `json:"database"`
	Host string `json:"host"`
	Port string `json:"port"`
}

// JSON
{
    "database": {
        "host": "localhost",
        "password": "12345"
    },
    "host": "localhost",
    "port": "8080"
}

*/
