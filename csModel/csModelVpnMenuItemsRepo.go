package csmodel

// VpnMenuItemsRepo serves the current VpnMenuItems repo.
type VpnMenuItemsRepo struct {
	VpnMenuItems []VpnMenuItem
}
