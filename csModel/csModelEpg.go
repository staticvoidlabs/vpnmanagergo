package csmodel

import "encoding/xml"

type EpgData struct {
	XMLName    xml.Name    `xml:"tv"`
	Programmes []Programme `xml:"programme"`
}

type Programme struct {
	XMLName    xml.Name `xml:"programme"`
	Channel    string   `xml:"channel,attr"`
	Start      string   `xml:"start,attr"`
	Stop       string   `xml:"stop,attr"`
	Title      string   `xml:"title"`
	Category   string   `xml:"category"`
	StartClean int
}
