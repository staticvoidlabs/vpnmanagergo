package main

import (
	"fmt"
	"time"

	Manager "./csManager"
	Models "./csModel"
	"github.com/veandco/go-sdl2/sdl"
)

var (
	currentConfig     Models.Config
	vpnMenuItemsRepo  Models.VpnMenuItemsRepo
	selectedItem      int
	shouldExit        bool
	shouldExitFast    bool
	ticks             uint32
	offsetOuterBorder int32
	offsetInnerBorder int32
)

func main() {

	go Manager.GetEpgData()

	// Init config.
	currentConfig = Manager.GetCurrentConfig()

	// Init SDL.
	Manager.InitSdlSubsysten(&currentConfig.SdlConfig)

	// Init shows repo.
	vpnMenuItemsRepo = Manager.InitVpnManagerMenuItemsRepo()
	var vpnMenuItemsSDL = make([]Models.SdlVpnMenuItem, len(vpnMenuItemsRepo.VpnMenuItems))
	selectedItem = 0

	shouldExit = false

	// Enter the main SDL loop.
	for {

		// Generate EPG textures.
		for i, item := range vpnMenuItemsRepo.VpnMenuItems {
			vpnMenuItemsSDL[i].IsSelected = false
			vpnMenuItemsSDL[i].Surface, _ = Manager.FontShowItem.RenderUTF8Blended(item.Text, sdl.Color{200, 200, 200, 200})
			vpnMenuItemsSDL[i].SurfaceSelected, _ = Manager.FontShowItem.RenderUTF8Blended(item.Text, sdl.Color{255, 255, 255, 255})
			vpnMenuItemsSDL[i].Texture, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].Surface)
			vpnMenuItemsSDL[i].TextureSelected, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].SurfaceSelected)

			if i == 0 {
				vpnMenuItemsSDL[i].SurfaceEpgNow, _ = Manager.FontHeaderVpnManager.RenderUTF8Blended(Manager.EpgOrf1Now, sdl.Color{255, 255, 255, 255})
				vpnMenuItemsSDL[i].SurfaceEpgNext, _ = Manager.FontHeaderVpnManager.RenderUTF8Blended(Manager.EpgOrf1Next, sdl.Color{255, 255, 255, 255})
				vpnMenuItemsSDL[i].TextureEpgNow, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].SurfaceEpgNow)
				vpnMenuItemsSDL[i].TextureEpgNext, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].SurfaceEpgNext)
			} else if i == 1 {
				vpnMenuItemsSDL[i].SurfaceEpgNow, _ = Manager.FontHeaderVpnManager.RenderUTF8Blended(Manager.EpgOrf2Now, sdl.Color{255, 255, 255, 255})
				vpnMenuItemsSDL[i].SurfaceEpgNext, _ = Manager.FontHeaderVpnManager.RenderUTF8Blended(Manager.EpgOrf2Next, sdl.Color{255, 255, 255, 255})
				vpnMenuItemsSDL[i].TextureEpgNow, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].SurfaceEpgNow)
				vpnMenuItemsSDL[i].TextureEpgNext, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].SurfaceEpgNext)
			} else if i == 2 {
				vpnMenuItemsSDL[i].SurfaceEpgNow, _ = Manager.FontHeaderVpnManager.RenderUTF8Blended(Manager.EpgOrf3Now, sdl.Color{255, 255, 255, 255})
				vpnMenuItemsSDL[i].SurfaceEpgNext, _ = Manager.FontHeaderVpnManager.RenderUTF8Blended(Manager.EpgOrf3Next, sdl.Color{255, 255, 255, 255})
				vpnMenuItemsSDL[i].TextureEpgNow, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].SurfaceEpgNow)
				vpnMenuItemsSDL[i].TextureEpgNext, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].SurfaceEpgNext)
			} else if i == 3 {
				vpnMenuItemsSDL[i].SurfaceEpgNow, _ = Manager.FontHeaderVpnManager.RenderUTF8Blended(Manager.EpgOrfSportPlusNow, sdl.Color{255, 255, 255, 255})
				vpnMenuItemsSDL[i].SurfaceEpgNext, _ = Manager.FontHeaderVpnManager.RenderUTF8Blended(Manager.EpgOrfSportPlusNext, sdl.Color{255, 255, 255, 255})
				vpnMenuItemsSDL[i].TextureEpgNow, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].SurfaceEpgNow)
				vpnMenuItemsSDL[i].TextureEpgNext, _ = Manager.Renderer.CreateTextureFromSurface(vpnMenuItemsSDL[i].SurfaceEpgNext)
			}
		}

		// Evaluate current event.
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {

			//switch event.(type) {
			switch t := event.(type) {

			case *sdl.QuitEvent:
				return

			case *sdl.KeyboardEvent:
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_ESCAPE {
					fmt.Println("Key: Esc")
					shouldExit = true
				}
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_LEFT {
					fmt.Println("Key: Left")
					selectedItem--
				}
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_RIGHT {
					fmt.Println("Key: Right")
					selectedItem++
				}
				if t.State == sdl.RELEASED && t.Keysym.Sym == sdl.K_f {
					fmt.Println("Key: f")
					if vpnMenuItemsRepo.VpnMenuItems[selectedItem].PlaybackUrl == "" {
						shouldExitFast = true
					}

					Manager.InitVpnConnection(vpnMenuItemsRepo.VpnMenuItems[selectedItem], currentConfig)
					shouldExit = true
				}
			}

		}

		if shouldExit {

			fmt.Println("Will exit now...")

			if shouldExitFast {
				time.Sleep(1 * time.Second)
			} else {
				time.Sleep(5 * time.Second)
			}

			Manager.QuitVpnManInstances()

			return
		}

		Manager.UpdateWindowPosition(currentConfig.SdlConfig.WindowsDelayFactor)

		Manager.Renderer.Clear()

		offsetOuterBorder = 20
		offsetInnerBorder = 10

		// Render Header.
		//surfaceHeader, _ := Manager.FontHeaderVpnManager.RenderUTF8Blended("VPN-Verbindungen", sdl.Color{200, 200, 200, 200})
		//textureHeader, _ := Manager.Renderer.CreateTextureFromSurface(surfaceHeader)
		//_, _, tmpW, tmpH, _ := textureHeader.Query()

		//Manager.Renderer.Copy(textureHeader, nil, &sdl.Rect{X: 20, Y: 10, W: tmpW, H: tmpH})

		// Render menu item textures.
		for i, _ := range vpnMenuItemsSDL {

			// Calucute position for texture.
			tmpStationBoxSize := (Manager.WindowWidth - 80) / 5
			tmpX := offsetOuterBorder + (int32(i) * tmpStationBoxSize) + (int32(i) * offsetInnerBorder)

			// Render gray/black background texture.
			if i == selectedItem {
				Manager.Renderer.SetDrawColor(200, 200, 200, 255)
			} else {
				Manager.Renderer.SetDrawColor(10, 10, 10, 255)
			}

			Manager.Renderer.FillRect(&sdl.Rect{X: tmpX, Y: 50, W: tmpStationBoxSize, H: 90})

			// Render white background texture.
			Manager.Renderer.SetDrawColor(255, 255, 255, 255)
			Manager.Renderer.FillRect(&sdl.Rect{X: tmpX + 5, Y: 55, W: tmpStationBoxSize - 10, H: 80})

			// Render station icon
			var tmpIconToUse string
			var tmpIconWidth int32
			var tmpIconHeight int32
			var tmpIconPosX int32
			var tmpIconPosY int32

			if Manager.WindowWidth < 880 {
				tmpIconToUse = "images/" + vpnMenuItemsRepo.VpnMenuItems[i].IconSmall
				tmpIconWidth = 100
				tmpIconHeight = 53
				tmpIconPosX = tmpX + ((tmpStationBoxSize - 100) / 2)
				tmpIconPosY = 67
			} else {
				tmpIconToUse = "images/" + vpnMenuItemsRepo.VpnMenuItems[i].Icon
				tmpIconWidth = 150
				tmpIconHeight = 80
				tmpIconPosX = tmpX + ((tmpStationBoxSize - 150) / 2)
				tmpIconPosY = 55
			}

			img, err := sdl.LoadBMP(tmpIconToUse)
			if err != nil {
				fmt.Println("Error loading station icon: ", err)
			}
			defer img.Free()

			tex, err := Manager.Renderer.CreateTextureFromSurface(img)
			if err != nil {
				fmt.Println("Error creating texture from station icon surface: ", err)
			}

			Manager.Renderer.Copy(tex, nil, &sdl.Rect{X: tmpIconPosX, Y: tmpIconPosY, W: tmpIconWidth, H: tmpIconHeight})

			// Render foreground texture.
			Manager.Renderer.SetDrawColor(10, 10, 10, 10)

			/*
				// Station name.
				var tmpTextureToRender *sdl.Texture

				if i == selectedItem {
					tmpTextureToRender = item.TextureSelected
				} else {
					tmpTextureToRender = item.Texture
				}

				Manager.Renderer.Copy(tmpTextureToRender, nil, &sdl.Rect{X: tmpX + 10, Y: 150, W: tmpW, H: tmpH})
			*/

			// EPG.
			if i == selectedItem {
				_, _, tmpWnow, tmpHnow, _ := vpnMenuItemsSDL[i].TextureEpgNow.Query()
				Manager.Renderer.Copy(vpnMenuItemsSDL[i].TextureEpgNow, nil, &sdl.Rect{X: currentConfig.SdlConfig.OffsetHeader + 10, Y: 180, W: tmpWnow, H: tmpHnow})

				_, _, tmpWnext, tmpHnext, _ := vpnMenuItemsSDL[i].TextureEpgNext.Query()
				Manager.Renderer.Copy(vpnMenuItemsSDL[i].TextureEpgNext, nil, &sdl.Rect{X: currentConfig.SdlConfig.OffsetHeader + 10, Y: 210, W: tmpWnext, H: tmpHnext})
			}

		}

		Manager.Renderer.Present()

	}
}
